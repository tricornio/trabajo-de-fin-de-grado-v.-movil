﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MainCamera : MonoBehaviour {
	
	// Update is called once per frame
	void Update () {
        Vector3 playerPos = GameObject.FindGameObjectWithTag("Player").transform.position;
        Vector3 camera = new Vector3(playerPos.x, 17f, playerPos.z);
        transform.position = camera;
    }

}
