﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovingEntity : MonoBehaviour {

    public float movementSpeed = 1f;
    public int rotationSpeed = 80;

    protected void MoveZombie (Vector3 direction, float speed) {

        noFly();

        Quaternion rotarHaciaObjetivo = Quaternion.LookRotation(direction, Vector3.up);
        // rotamos desde la posicion actual, hacia la objetivo, a X velocidad
        transform.rotation = Quaternion.Lerp(transform.rotation, rotarHaciaObjetivo, rotationSpeed * Time.deltaTime);
        // luego nos movemos (aunque no entiendo muy bien como???)
        transform.position += transform.forward * speed * Time.deltaTime;
    }

    protected void MovePlayer(Vector3 direction){

        noFly();

        // http://wiki.unity3d.com/index.php?title=LookAtMouse
        // Generate a plane that intersects the transform's position with an upwards normal.
        /*var playerPlane = new Plane(Vector3.up, transform.position);

        // Generate a ray from the cursor position
        var ray = Camera.main.ScreenPointToRay(Input.mousePosition);

        // Determine the point where the cursor ray intersects the plane.
        // This will be the point that the object must look towards to be looking at the mouse.
        // Raycasting to a Plane object only gives us a distance, so we'll have to take the distance,
        //   then find the point along that ray that meets that distance.  This will be the point
        //   to look at.
        float hitdist = 0;
        // If the ray is parallel to the plane, Raycast will return false.
        if (playerPlane.Raycast(ray, out hitdist)) {
            // Get the point along the ray that hits the calculated distance.
            var targetPoint = ray.GetPoint(hitdist);

            // Determine the target rotation.  This is the rotation if the transform looks at the target point.
            var targetRotation = Quaternion.LookRotation(targetPoint - transform.position);

            // Smoothly rotate towards the target point.
            transform.rotation = Quaternion.Lerp( transform.rotation, targetRotation, rotationSpeed * Time.deltaTime);
        }*/
        
        transform.position += direction * 5 * Time.deltaTime;
    }

    public void noFly() {
        Vector3 noFly = transform.position;
        noFly.y = 0;
        transform.position = noFly;
    }

}
