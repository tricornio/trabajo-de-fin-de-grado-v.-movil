﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class Health : MonoBehaviour {

    public float health;
    private float lastBloodEffect = 0;
    private float bloodCooldown = 0.3f;
    public float maxHealth;
    private bool arrastrarse = false;
    private bool escopeta = false;
    private int contador = 0;
    private float criticalDamage = 1;
    private GameObject player;
    public ParticleSystem bloodEffect;
    public ParticleSystem shootBloodEffect;
    private ParticleSystem stopBlood;
    private ParticleSystem stopBloodShoot;

    void DamageTaken( float damage ){
        health -= damage;
        Debug.Log( "Vida actual: " + health );

        if( gameObject.name.ToString().Contains( "Player" ) && health <= 0 )
            gameOverScene();

        if( gameObject.name.ToString().Contains( "Obstacle" ) ) {
            if( health <= 0 )
                destroyObstacle();
            else
                lightEffect();
        }

        if( !arrastrarse && !gameObject.name.ToString().Contains( "Player" ) && !gameObject.name.ToString().Contains( "Obstacle" ) && player ) {
            if( player.GetComponent<Player>().weaponSelector == 1 )
                contador++;

            if( contador <= 1 ){
                shootBlood();
                if( UnityEngine.Random.Range( 0, 7 ) == 4 ) {
                    player.GetComponent<Player>().criticalDamage.Play();
                    health -= criticalDamage;
                }
            }
        }

        if( gameObject.name.ToString().Contains( "Player" ) && health > 0 )
            GetComponent<Player>().receiveDamage.Play();

        if( health <= 0 && gameObject.name.ToString().Contains( "Zombie" ) ) {
            if( arrastrarse == true )
                Invoke("stopAnimation", 0.06f); //aqui metemos el invoke porque con la escopeta sino peta. Ya que tiene 0.05 de tiempo antes de parar
            Invoke( "destroyZombie", 0.06f ); 
        }

        if( health <= 0 && gameObject.name.ToString().Contains( "Alerter" ) )
            Invoke( "destroyAlerter", 0.06f );

        // efecto de arrastrarse por el suelo
        if( gameObject.name.ToString().Contains("Zombie") && health > 0 ) {
            if( UnityEngine.Random.Range( 0, 6 ) == 5 ){
                arrastrarse = true;
                GetComponent<Zombie>().animations.SetBool( "arrastrarse", true );
                GetComponent<Zombie>().speed = 0.7f;
                StartCoroutine( bloodCoroutine() );
            }
        }
    }

    private void destroyObstacle() {
        GetComponent<Obstacle>().deadEffect();
    }

    private void lightEffect(){
        GetComponent<Obstacle>().lightEffect();
    }

    private void shootBlood() {
        // posicion de la sangre y rotacion
        Vector3 rotationBlood = transform.rotation.eulerAngles;
        rotationBlood.y = rotationBlood.y * -1;
        Vector3 positionBlood = transform.position;
        positionBlood.y = -0.53f;

        stopBloodShoot = ParticleSystem.Instantiate( shootBloodEffect, positionBlood, Quaternion.Euler( rotationBlood ) );
        stopBloodShoot.Play();
        Invoke( "pauseBloodShootEffect", 0.05f );
    }

    private void pauseBloodShootEffect() {
        stopBloodShoot.Pause();
    }

    IEnumerator bloodCoroutine(){
        while( health > 0 ) {
            if( Time.time - lastBloodEffect > bloodCooldown ){
                lastBloodEffect = Time.time;

                // posicion de la sangre y rotacion
                Vector3 rotationBlood = transform.rotation.eulerAngles;
                //rotationBlood.y = rotationBlood.y * -1;
                Vector3 positionBlood = transform.position;
                //positionBlood.y = 0f;

                stopBlood = ParticleSystem.Instantiate( bloodEffect, positionBlood, Quaternion.Euler( rotationBlood ) );
                stopBlood.Play();
                Invoke( "stopBloodEffect", 0.05f );
            }
            yield return 0;
        }
    }

    private void stopBloodEffect() {
        stopBlood.Pause();
    }

    private void stopAnimation() {
        GetComponent<Zombie>().animations.SetBool( "dead", true );
        GetComponent<Zombie>().audio[0].Stop();
        GetComponent<Zombie>().audio[1].Stop();
        Destroy( gameObject.GetComponents<NavMeshAgent>()[0] );
        Destroy( gameObject.GetComponents<Zombie>()[0] );
        Destroy( gameObject.GetComponents<Health>()[0] );
    }

    private void destroyZombie() {
        int animation = UnityEngine.Random.Range( 1, 7 );
        string deadAnimation = "dead" + animation.ToString();

        if( arrastrarse == false ) {
            GetComponent<Zombie>().animations.SetBool( deadAnimation, true );
            GetComponent<Zombie>().audio[0].Stop();
            GetComponent<Zombie>().audio[1].Stop();
            Destroy( gameObject.GetComponents<NavMeshAgent>()[0] );
            Destroy( gameObject.GetComponents<Zombie>()[0] );
            Destroy( gameObject.GetComponents<Health>()[0] );
        }

        gameObject.transform.position = new Vector3( transform.position.x, -0.1f, transform.position.z );
        Destroy( gameObject.GetComponents<damageController>()[0] );
        Destroy( gameObject.GetComponents<Rigidbody>()[0] );
        Destroy( gameObject.GetComponents<BoxCollider>()[0] );
    }

    private void destroyAlerter(){
        int animation = UnityEngine.Random.Range( 1, 7 );
        string deadAnimation = "dead" + animation.ToString();
        GetComponent<AlerterZombie>().animations.SetBool( deadAnimation, true );

        Destroy( gameObject.GetComponents<AlerterZombie>()[0] );
        Destroy( gameObject.GetComponents<Health>()[0] );
        Destroy( gameObject.GetComponents<NavMeshAgent>()[0] );
        Destroy( gameObject.GetComponents<Rigidbody>()[0] );
        Destroy( gameObject.GetComponents<BoxCollider>()[0] );
        Destroy( gameObject.GetComponents<AudioSource>()[0] );
        Destroy( gameObject.GetComponents<AudioSource>()[1] );
    }

    void Start() {
        player = GameObject.Find( "Player" );
    }

    private void gameOverScene() {
        player.GetComponent<Player>().deadSound.Play();
        Destroy( player.GetComponent<MeshRenderer>() );
        Instantiate( player.GetComponent<Player>().deadEffect, new Vector3(transform.position.x, 2, transform.position.z), Quaternion.Euler( 90, 0, 145 ) );
        player.GetComponent<Player>().range.gameObject.transform.localScale = new Vector3(0,0,0);
        Destroy( gameObject.GetComponents<Player>()[0] );
        //player.SetActive( false );
        Invoke( "changeToGameOverScene", 1.5f );
    }

    private void changeToGameOverScene() {
        Application.LoadLevel( "GameOverScene" );
    }

}
