﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ZombieGenerator : MonoBehaviour {

    public GameObject zombiePrefab1;
    public GameObject zombiePrefab2;
    public GameObject zombiePrefab3;
    public GameObject zombiePrefab4;
    public GameObject zombiePrefab5;
    private List<GameObject> allZombies = new List<GameObject>();
    public float spawnTime = 0;
    public float quanty = 20;
    public float spawnRadius = 5;
    private bool spawn = true;

    GameObject zombi;
    float lastSizeTime = 0;
    float sizeCooldown = 0.05f;

    void GenerateZombie(){
        Vector3 randomPosition = transform.position + Random.insideUnitSphere * spawnRadius;
        randomPosition.y = 0;

        int zombie = UnityEngine.Random.Range( 1, 6 );

        if( zombie == 1 )
            allZombies.Add( zombi = GameObject.Instantiate( zombiePrefab1, transform.position, Quaternion.identity ) );
        if( zombie == 2 )
            allZombies.Add( zombi = GameObject.Instantiate( zombiePrefab2, transform.position, Quaternion.identity ) );
        if( zombie == 3 )
            allZombies.Add( zombi = GameObject.Instantiate( zombiePrefab3, transform.position, Quaternion.identity ) );
        if( zombie == 4 )
            allZombies.Add( zombi = GameObject.Instantiate( zombiePrefab4, transform.position, Quaternion.identity ) );
        if( zombie == 5 )
            allZombies.Add( zombi = GameObject.Instantiate( zombiePrefab5, transform.position, Quaternion.identity ) );

        zombi.transform.localScale = new Vector3( 0.1f, 0.1f, 0.1f );
    }

    // Update is called once per frame
    void Update(){
        foreach( GameObject zombie in allZombies.ToArray() ) {
            if( !zombie.GetComponent<Health>() ) {
                allZombies.Remove( zombie );
            }
        }

        if( zombi && Time.time - lastSizeTime > sizeCooldown && zombi.transform.localScale.x < 1 ) {
            lastSizeTime = Time.time;
            zombi.transform.position = transform.position;
            zombi.transform.localScale += new Vector3( 0.05f, 0.05f, 0.05f );
        }

    }

    void Start(){
        StartCoroutine( Spawner() );
    }

    void OnTriggerStay( Collider other ){
        if( other.CompareTag( "Player" ) ) {
            spawn = false;
        }
    }

    void OnTriggerExit( Collider other ){
        if( other.CompareTag( "Player" ) ) {
            spawn = true;
        }
    }

    IEnumerator Spawner(){
        while( true ){
            if( allZombies.Count <= quanty && spawn )
                GenerateZombie();

            yield return new WaitForSeconds( spawnTime );
        }
    }

}