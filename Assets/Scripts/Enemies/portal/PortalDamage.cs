﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PortalDamage : MonoBehaviour {

    Transform damageArea;
    bool playerExit;
    private AudioSource closePortal;
    bool musicOn;
    private GameObject level2Objetives;
    private bool destroyPortal = false;

    void Awake () {
        level2Objetives = GameObject.FindGameObjectWithTag( "Level2Objetives" );
        damageArea = transform.Find( "damageRange" );
        closePortal = GetComponent<AudioSource>();
	}

    void Update() {
        if( playerExit && damageArea.localScale.x > 0 )
            damageArea.localScale = damageArea.localScale - new Vector3( 0.005f, 0.005f, 0f );
    }
	
	void OnTriggerStay( Collider other ){
        if( damageArea && other.CompareTag( "Player" ) ) {
            playerExit = false; ;
            damageArea.localScale = damageArea.localScale + new Vector3( 0.005f, 0.005f, 0f );
        }

        if( damageArea && damageArea.localScale.x >= 10 && !destroyPortal ) {
            destroyPortal = true;
            destroyChilds();
        }

        if( damageArea && damageArea.localScale.x >= 9.70 && musicOn ) {
            closePortal.Play();
            musicOn = false;
        }

    }

    void OnTriggerEnter( Collider other ){
        if( other.CompareTag( "Player" ) )
            musicOn = true;
    }

    void OnTriggerExit( Collider other ){
        if( other.CompareTag( "Player" ) ) {
            playerExit = true;
            musicOn = false;
            closePortal.Stop();
        }
    }

    void destroyChilds() {
        foreach (Transform child in transform) {
            if( child.tag == "lava" ) 
                child.gameObject.SetActive( true );
            else
                GameObject.Destroy( child.gameObject );
        }
        level2Objetives.GetComponent<LevelTwoObjectives>().closedPortal += 1;
        Destroy( gameObject.GetComponent<PortalDamage>() );
        Destroy( gameObject.GetComponent<ZombieGenerator>() );
        this.transform.parent.gameObject.SetActive( false );
    }

}
