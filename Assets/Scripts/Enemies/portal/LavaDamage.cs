﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LavaDamage : MonoBehaviour {

    private float damageCooldown = 1;
    private float lastDamageTime = 0;
    private float damage = 0;

    void OnTriggerStay( Collider other ){
        if( other.CompareTag( "Player" ) || other.CompareTag( "Enemy" ) ) {
            if( other.CompareTag( "Player" ) )
                damage = 0.25f;
            else
                damage = 0.5f;

            if( Time.time - lastDamageTime > damageCooldown ) {
                lastDamageTime = Time.time;
                other.SendMessage( "DamageTaken", damage );
            }
        }
    }
    
}
