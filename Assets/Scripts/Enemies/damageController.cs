﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class damageController : MonoBehaviour {

    private float lastDamageTime;
    private float damageCooldown;
    private float damage;
    private Transform playerPosition;
    //private Color32 lastColor;

    // Use this for initialization
    void Start () {
        damageCooldown = 2f;
        lastDamageTime = 0;
        damage = transform.root.gameObject.GetComponent<Zombie>().damage;
        //lastColor = GetComponent<Renderer>().material.color;
    }

    void OnTriggerStay( Collider other ){
        if( other.CompareTag( "Player" ) ){
            playerPosition = transform.root.gameObject.GetComponent<Zombie>().playerPosition;
            if( (playerPosition.position - transform.position).magnitude <= 2f ){
                doDamage( other );
            }
        }
    }

    void doDamage( Collider other ){
        if( Time.time - lastDamageTime > damageCooldown ){
            lastDamageTime = Time.time;
            Debug.Log( "El daño que te hace el enemigo es de: " + damage );
            StartCoroutine( attackEffect( other ) );

            // damage, origin, destiny
            //object[] damageData = new object[3] { damage, "zombie", "player"};
            other.SendMessage( "DamageTaken", damage );
        }
    }





    IEnumerator attackEffect( Collider other ){
        //GetComponent<Renderer>().material.color = new Color32( 255, 0, 0, 1 );
        Vector3 originalPosition = transform.position;
        Vector3 attackPosition = other.transform.position;
        float attackspeed = 2;
        float percent = 0;

        while( percent <= 1 ){
            percent += Time.deltaTime * attackspeed;
            float interpolation = (-Mathf.Pow( percent, 2 ) + percent) * 4;
            transform.position = Vector3.Lerp( originalPosition, attackPosition, interpolation ); 
            yield return null;
        }

        //GetComponent<Renderer>().material.color = lastColor;

    }





}
