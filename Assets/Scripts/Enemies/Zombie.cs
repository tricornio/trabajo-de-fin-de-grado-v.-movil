﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class Zombie : MovingEntity{

    public float areaVagabundeo = 5f;
    public float damage = 2.5f;
    public float speed = 3.5f;

    public Vector3 newPos;
    Vector3 posicionObjetivo;
    Vector3 lastPosicionObjetivo;
    Vector3 distanciaObjetivo;
    float[] mapSize = new float[2];
    public bool onPosition = false;
    public bool prepareToCrowdAttack = false;
    public float crowdSpeed = 3.5f;

    public AudioSource[] audio;
    public Animator animations;
    public Animation actualAnimation;
    public bool alerterNorice = false;

    public state currentState;
    public Transform playerPosition;
    float maxChaseDistance = 15f;
    public NavMeshAgent pathfinder;
    
    public enum state{
        Chase,
        Wander,
        CrowdAttack,
    }

    IEnumerator stateMachine(){
        while( true ){
            yield return StartCoroutine( currentState.ToString() );
        }
    }

    IEnumerator Wander(){
        while( currentState == state.Wander ){
            //recalculas distancia hacia el punto objetivo
            distanciaObjetivo = posicionObjetivo - transform.position;
            pathfinder.speed = 1;
            pathfinder.SetDestination( newPos );

            // si ya llegaste al objetivo recalculamos un nuevo objetivo
            if( distanciaObjetivo.magnitude <= 2f )
                RecalculateTargetPosition();

            Debug.DrawLine( transform.position, posicionObjetivo, Color.blue );
            yield return 0;
        }
    }

    IEnumerator Chase(){
        while( currentState == state.Chase ){
            posicionObjetivo = playerPosition.position - transform.position;
            pathfinder.speed = speed;
            pathfinder.SetDestination( playerPosition.position );
            //MoveZombie( posicionObjetivo, 0 );

            if( posicionObjetivo.magnitude >= maxChaseDistance && alerterNorice == false ){
                currentState = state.Wander;
                animations.SetBool( "detected", false );
                audio[1].Stop();
                audio[0].Play();
                RecalculateTargetPosition();
            }

            yield return 0;
        }
    }

    // Calcular una posicion a la que moverse, para que el zombie se mueva por ahi aleatoriamente
    void RecalculateTargetPosition(){
        lastPosicionObjetivo = posicionObjetivo;
        posicionObjetivo = transform.position + Random.insideUnitSphere * areaVagabundeo;
        posicionObjetivo.y = 0; // para no volar

        if( (posicionObjetivo.x < mapSize[0] && posicionObjetivo.x > mapSize[1]) && (posicionObjetivo.z < mapSize[0] && posicionObjetivo.z > mapSize[1]) ){
            NavMeshHit navHit;
            NavMesh.SamplePosition( posicionObjetivo, out navHit, areaVagabundeo, -1 );
            newPos = navHit.position;
        }else { posicionObjetivo = lastPosicionObjetivo; }
        
    }

    // Use this for initialization
    void Start(){
        currentState = state.Wander;
        pathfinder = GetComponent<NavMeshAgent>();
        GetComponent<Health>().health = 5;

        mapSize[0] = GameObject.FindGameObjectWithTag( "SubMapLevel2" ).GetComponent<MapSize>().mapSize.bounds.max.x;
        mapSize[1] = GameObject.FindGameObjectWithTag( "SubMapLevel2" ).GetComponent<MapSize>().mapSize.bounds.min.x;
        RecalculateTargetPosition();
        StartCoroutine( stateMachine() );

        animations = GetComponent<Animator>();
        audio = GetComponents<AudioSource>();
        audio[0].Play();
    }

    void OnTriggerEnter( Collider other ){
        if( other.CompareTag( "Player" ) ){
            audio[0].Stop();
            audio[1].Play();
            animations.SetBool( "detected", true );
            currentState = state.Chase;
            playerPosition = other.transform;
        }
    }

    IEnumerator CrowdAttack(){
        while( currentState == state.CrowdAttack ){

            if( prepareToCrowdAttack )
                newPos = GameObject.FindGameObjectWithTag( "Player" ).transform.position;

            pathfinder.speed = crowdSpeed;
            pathfinder.SetDestination( newPos );

            if( newPos.magnitude - transform.position.magnitude <= 2f )
                onPosition = true;

            yield return 0;
        }
    }
    public float zombieGoalSpeed;

    void Update() {
        gameObject.transform.position = new Vector3( transform.position.x, 0f, transform.position.z );
    }

}
