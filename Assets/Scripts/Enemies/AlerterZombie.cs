﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class AlerterZombie : MovingEntity{

    public float areaVagabundeo = 5f;
    public float damage = 2.5f;

    Vector3 newPos;
    Vector3 posicionObjetivo;
    Vector3 distanciaObjetivo;
    Vector3 lastPosicionObjetivo;
    float[] mapSize = new float[2];

    public Animator animations;
    AudioSource[] audio;
    private bool stop = true;
    private bool gritar = false;

    state currentState;
    public Transform playerPosition;
    float maxChaseDistance = 15f;
    private NavMeshAgent pathfinder;

    private enum state{
        Detected,
        Wander
    }

    IEnumerator stateMachine(){
        while( true ){
            yield return StartCoroutine( currentState.ToString() );
        }
    }

    IEnumerator Wander(){
        while( currentState == state.Wander ){
            //recalculas distancia hacia el punto objetivo
            distanciaObjetivo = posicionObjetivo - transform.position;
            if( stop ) {
                //MoveZombie( distanciaObjetivo.normalized, 1f );
                pathfinder.speed = 1;
                pathfinder.SetDestination( newPos );
            }

            if( UnityEngine.Random.Range( 0, 2500 ) == 8 ){
                stop = false;
                pathfinder.SetDestination( transform.position );
                animations.SetBool( "merodear", true );
                Invoke( "startWalking", 4.6f );
            }

            // si ya llegaste al objetivo recalculamos un nuevo objetivo
            if( distanciaObjetivo.magnitude <= 2f ) //ANTES 0.5F
                RecalculateTargetPosition();

            Debug.DrawLine( transform.position, posicionObjetivo, Color.blue );
            yield return 0;
        }
    }

    private void startWalking() {
        stop = true;
        animations.SetBool( "merodear", false );
    }

    IEnumerator Detected(){
        while( currentState == state.Detected ){
            posicionObjetivo = playerPosition.position - transform.position;
            // este no ataca solo llama
            MoveZombie( posicionObjetivo, 0 );

            if( posicionObjetivo.magnitude >= maxChaseDistance ){
                gritar = false;
                currentState = state.Wander;
                animations.SetBool( "detected", false );
                audio[1].Stop();
                audio[0].Play();
                RecalculateTargetPosition();
            }

            yield return 0;
        }
    }

    // Calcular una posicion a la que moverse, para que el zombie se mueva por ahi aleatoriamente
    void RecalculateTargetPosition(){
        lastPosicionObjetivo = posicionObjetivo;
        posicionObjetivo = transform.position + Random.insideUnitSphere * areaVagabundeo;
        posicionObjetivo.y = 0; // para no volar

        if( (posicionObjetivo.x < mapSize[0] && posicionObjetivo.x > mapSize[1]) && (posicionObjetivo.z < mapSize[0] && posicionObjetivo.z > mapSize[1]) ){
            NavMeshHit navHit;
            NavMesh.SamplePosition( posicionObjetivo, out navHit, areaVagabundeo, -1 );
            newPos = navHit.position;
        }else { posicionObjetivo = lastPosicionObjetivo; }
    }

    // Use this for initialization
    void Start(){
        pathfinder = GetComponent<NavMeshAgent>();
        mapSize[0] = GameObject.FindGameObjectWithTag( "SubMapLevel2" ).GetComponent<MapSize>().mapSize.bounds.max.x;
        mapSize[1] = GameObject.FindGameObjectWithTag( "SubMapLevel2" ).GetComponent<MapSize>().mapSize.bounds.min.x;
        currentState = state.Wander;
        GetComponent<Health>().health = 5;
        RecalculateTargetPosition();

        StartCoroutine( stateMachine() );
        animations = GetComponent<Animator>();
        audio = GetComponents<AudioSource>();
        audio[0].Play();
    }

    void OnTriggerEnter( Collider other ){
        if( other.CompareTag( "Player" ) ){
            playerPosition = other.transform;
            pathfinder.SetDestination( transform.position );
            gritar = true;
            animations.SetBool( "detected", true );
            audio[0].Stop();
            audio[1].Play();
            currentState = state.Detected;
        }
    }

    void OnTriggerStay( Collider other ) {
        if( other.CompareTag( "Enemy" ) && gritar && other.name.ToString().Contains( "Zombie" ) ){
            other.GetComponent<Zombie>().playerPosition = playerPosition;
            other.GetComponent<Zombie>().animations.SetBool( "detected", true );
            other.GetComponent<Zombie>().alerterNorice = true;
            other.GetComponent<Zombie>().currentState = Zombie.state.Chase;
            other.GetComponent<Zombie>().audio[0].Stop();
            other.GetComponent<Zombie>().audio[1].Stop();
        }
    }

    void OnTriggerExit( Collider other ) {
        if( other.CompareTag( "Enemy" ) ){
            if( other.name.ToString().Contains( "Zombie" ) && other.GetComponent<Zombie>().alerterNorice == true )
                other.GetComponent<Zombie>().alerterNorice = false;
        }
    }

}
