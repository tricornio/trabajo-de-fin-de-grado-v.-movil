﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ZombieDogController : MonoBehaviour {

    List<Collider> onTriggerZombies = new List<Collider>();
    List<Collider> onPositionZombies = new List<Collider>();

    List<Vector3> conersMap = new List<Vector3>();
    float[] mapSize = new float[2];

    float searchCooldown = 25f;
    float lastSearch = 0;
    bool setPositions = false;

    float attackCooldown = 20f;
    float lastAttack = 0;
    bool addzombie = true;

    bool restartSpeed = true;
    bool calculatespeed = true;

    // falta rellenar las esquinas
    void Start(){
        mapSize[0] = GameObject.FindGameObjectWithTag( "SubMapLevel2" ).GetComponent<MapSize>().mapSize.bounds.max.x;
        mapSize[0] -= 4; // 44 
        mapSize[1] = GameObject.FindGameObjectWithTag( "SubMapLevel2" ).GetComponent<MapSize>().mapSize.bounds.min.x;
        mapSize[1] += 4; // -48

        conersMap.Add( new Vector3( mapSize[0], 0, mapSize[1] ) );
        conersMap.Add( new Vector3( mapSize[1], 0, mapSize[1] ) );
        conersMap.Add( new Vector3( mapSize[0], 0, mapSize[0] ) );
        conersMap.Add( new Vector3( mapSize[1], 0, mapSize[0] ) );
    }

    void Update(){
        // movemos los zombis a las esquinas correspondientes
        if( Time.time - lastSearch > searchCooldown ){
            lastSearch = Time.time;

            setPositions = true;
            if( onTriggerZombies.Count >= 8 && !anyZombieAttack() ) {
                setPosition();
            }
            Invoke( "positionsPause", 3f );
        }

        // metemos los que estan en las esquinas en OnpositionZombies
        if( Time.time - lastAttack > attackCooldown && addzombie ){
            lastAttack = Time.time;
            
            foreach( Collider zombie in onTriggerZombies.ToArray() ){
                if( !zombie )
                    onTriggerZombies.Remove( zombie );
                if( zombie && zombie.GetComponent<Zombie>().onPosition ) {
                    onPositionZombies.Add( zombie );
                    onTriggerZombies.Remove( zombie );
                }
            }
        }

        // check zombie status (is in wander mode?)
        checkStatus();

        if( onPositionZombies.Count >= 8 ) {
            addzombie = false;
            attack();
        }

    }

    private bool anyZombieAttack() {
        foreach( Collider zombie in onPositionZombies ){
            if( zombie && zombie.GetComponent<Zombie>().prepareToCrowdAttack )
                return true;
        }
        return false;
    }

    private void positionsPause() { setPositions = false;  }

    private void checkStatus(){
        foreach( Collider zombie in onPositionZombies.ToArray() ){
            if( !zombie )
                onPositionZombies.Remove( zombie );
            if( zombie && zombie.GetComponent<Zombie>().currentState == Zombie.state.Wander ) {
                zombie.GetComponent<Zombie>().prepareToCrowdAttack = false;
                zombie.GetComponent<Zombie>().onPosition = false;
                onPositionZombies.Remove( zombie );
            }
        }

        if( onPositionZombies.Count == 0 ) {
            calculatespeed = true;
            addzombie = true;
        }
            

    }

    private void attack() {
        int dead = 0;

        foreach( Collider zombie in onPositionZombies ){
            if( !zombie )
                dead++;
            else
                zombie.GetComponent<Zombie>().prepareToCrowdAttack = true;
        }

        if( calculatespeed ) {
            calculatespeed = false;
            Invoke( "coroutineToSpeed", 0.5f );
        }

        if( dead == onPositionZombies.Count ){
            onPositionZombies.Clear();
            addzombie = true;
        }
        
    }

    private void coroutineToSpeed() { StartCoroutine( "calculateSpeed" ); }

    IEnumerator calculateSpeed() {

        while( anyZombieAttack() ){
            // obtenemos el zombie mas alejado
            Collider farZombie = null;
            int distance = -1;
            foreach( Collider zombie in onPositionZombies ){
                if( zombie && zombie.GetComponent<Zombie>().prepareToCrowdAttack ) {
                    if( (int)zombie.GetComponent<Zombie>().pathfinder.remainingDistance > distance ) {
                        farZombie = zombie;
                        distance = (int)zombie.GetComponent<Zombie>().pathfinder.remainingDistance;
                    }
                }
            }
            
            int goalSpeed = (int)(farZombie.GetComponent<Zombie>().pathfinder.remainingDistance / farZombie.GetComponent<Zombie>().crowdSpeed);
            foreach( Collider zombie in onPositionZombies ){
                if( zombie && zombie.GetComponent<Zombie>().prepareToCrowdAttack ) {
                    int zombieGoalSpeed = (int)( zombie.GetComponent<Zombie>().pathfinder.remainingDistance / zombie.GetComponent<Zombie>().crowdSpeed );
                    zombie.GetComponent<Zombie>().zombieGoalSpeed = goalSpeed - zombieGoalSpeed;
                    if( goalSpeed - zombieGoalSpeed > 1f ) {
                        if( zombie != farZombie ) { 
                            zombie.GetComponent<Zombie>().crowdSpeed -= 0.5f;
                        }
                    }
                }
            }

            if( restartSpeed ) {
                restartSpeed = false;
                Invoke( "resetSpeed", 1f );
            }

            yield return 0;

        }

    }

    private void resetSpeed(){
        foreach( Collider zombie in onPositionZombies ){
            if( zombie && zombie.GetComponent<Zombie>().prepareToCrowdAttack ) { 
                if( zombie && zombie.GetComponent<Zombie>().crowdSpeed < 0.5f )
                    zombie.GetComponent<Zombie>().crowdSpeed = zombie.GetComponent<Zombie>().crowdSpeed = 3.5f;
            }
        }
        restartSpeed = true;
    }

    void OnTriggerStay( Collider other ){
        if( other.name.ToString().Contains( "Zombie" ) ) {
            if( !onTriggerZombies.Contains( other ) && setPositions )
                onTriggerZombies.Add( other );
        }
    }

    private void setPosition() {

        foreach( Collider zombie in onTriggerZombies.ToArray() ) {
            if( zombie && zombie.GetComponent<Zombie>().currentState != Zombie.state.CrowdAttack ) {
                zombie.GetComponent<Zombie>().newPos = conersMap[UnityEngine.Random.Range( 0, 4 )] + Random.insideUnitSphere * 3;
                zombie.GetComponent<Zombie>().currentState = Zombie.state.CrowdAttack;
            }
            if( !zombie )
                onTriggerZombies.Remove( zombie );
        }

    }

}
