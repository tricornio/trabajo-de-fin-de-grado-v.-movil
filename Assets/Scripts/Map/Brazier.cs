﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Brazier : MonoBehaviour {

    private float lastSoundTime = 0;
    private float SoundCooldown = 42;
    private AudioSource[] sound;
    private bool playSound = true;

    // Use this for initialization
    void Start () {
        sound = gameObject.GetComponents<AudioSource>();
	}
	
	// Update is called once per frame
	void Update () {
        if( Time.time - lastSoundTime > SoundCooldown && playSound ) {
            playSound = false;
            sound[1].Play();
        }
	}
}
