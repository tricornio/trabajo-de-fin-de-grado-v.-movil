﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Obstacle : MonoBehaviour {

    public ParticleSystem deadAnimation;
    private ParticleSystem persistente;
    private bool noEnter = false;
    private AudioSource[] sound;
    private float lightCooldown = 1f;
    private float lastLightEffect = 0;
    private Light light;

    void Start() {
        GetComponent<Health>().health = 3;
        GetComponent<Health>().maxHealth = 3;
        sound = GetComponents<AudioSource>();
        light = GetComponentInChildren<Light>();
    }

    public void lightEffect() {
        if( UnityEngine.Random.Range( 0, 4 ) == 3 ) {
            StartCoroutine( onOffInfinite() );
        }
    }

    IEnumerator onOffInfinite(){
        while( true ){
            if( Time.time - lastLightEffect > lightCooldown ){
                lastLightEffect = Time.time;

                if( light.enabled == true ){
                    light.enabled = false;
                }else{
                    sound[1].Play();
                    light.enabled = true;
                }    

            }
            yield return 0;
        }
    }

    public void deadEffect() {
        if( !noEnter ){
            noEnter = true;
            destroyMats();

            Vector3 positionBlood = transform.position;
            positionBlood.y = 15f;
            Vector3 rotationBlood = transform.rotation.eulerAngles;
            rotationBlood.x = 90f;
            persistente = Instantiate( deadAnimation, transform.position, Quaternion.Euler( rotationBlood ) );
            persistente.Play();
            sound[0].Play();

            Invoke( "stopEffect", 0.8f );
        }
    }

    private void stopEffect() {
        persistente.Pause();
        Destroy( gameObject );
    }

    private void destroyMats(){
        foreach( Transform child in transform )
            GameObject.Destroy( child.gameObject );

        Destroy( gameObject.GetComponents<MeshRenderer>()[0] );
        Destroy( gameObject.GetComponents<BoxCollider>()[0] );
    }

}
