﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MapSize : MonoBehaviour {

    public Renderer mapSize;

    void Awake () {
        mapSize = gameObject.GetComponent<Renderer>();
        Debug.Log( mapSize.bounds.max );
        Debug.Log( mapSize.bounds.min );
    }
}
