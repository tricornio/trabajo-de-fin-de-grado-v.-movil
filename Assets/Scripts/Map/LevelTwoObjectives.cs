﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LevelTwoObjectives : MonoBehaviour {

    public int closedPortal = 0;
    private GameObject textProgress;
    public GameObject objectivesCanvas;
    public GameObject apuntesCanvas;
    public GameObject controlsCanvas;

    void Awake() {
        textProgress = GameObject.FindGameObjectWithTag( "cloasedPortals" );
        objectivesCanvas = GameObject.FindGameObjectWithTag( "ObjectivesCanvas" );
        apuntesCanvas = GameObject.FindGameObjectWithTag( "ApuntesCanvas" );
        controlsCanvas = GameObject.FindGameObjectWithTag( "ControlsCanvas" );
        objectivesCanvas.SetActive( false );
        apuntesCanvas.SetActive( false );
        controlsCanvas.SetActive( false );
    }

	void LateUpdate () {
        if( closedPortal >= 4 ) {
            Application.LoadLevel( "TheEnd" );
        }
        textProgress.GetComponent<Text>().text = closedPortal + " / 4";
    }

    public void objectivesCanvasActivate() {
        objectivesCanvas.SetActive( true );
    }

    public void apuntesCanvasActivate() {
        apuntesCanvas.SetActive( true );
    }

    public void controlsCanvasActivate() {
        controlsCanvas.SetActive( true );
    }

}
