﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class changeWeapon : MonoBehaviour {

    GameObject player;

	void Awake () {
        player = GameObject.FindGameObjectWithTag( "Player" );
	}

    public void changeWeapons() {
        player.GetComponent<Player>().weaponChange();
    }

}
