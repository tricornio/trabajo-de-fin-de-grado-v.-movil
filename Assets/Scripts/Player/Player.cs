﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MovingEntity {
    
    Weapon[] weapons;
    public GameObject range;
    private GameObject secondRange;
    public ParticleSystem deadEffect;
    public AudioSource receiveDamage;
    public AudioSource criticalDamage;
    public AudioSource deadSound;
    public int weaponSelector = 0;
    public Vector3 desiredDirection = Vector3.zero;

    void Start(){
        //GetComponent<Health>().health = 35;
        //GetComponent<Health>().maxHealth = 35;
        weapons = GetComponentsInChildren<Weapon>();
        range = GameObject.Find( "weaponRange" );
        secondRange = GameObject.Find( "secondWeaponRange" ); 
        receiveDamage = GetComponents<AudioSource>()[0];
        criticalDamage = GetComponents<AudioSource>()[1];
        deadSound = GetComponents<AudioSource>()[2];
        weaponChange();
    }

	// Update is called once per frame
	void Update () {

        //Vector3 desiredDirection = Vector3.zero;
        if (Input.GetKey(KeyCode.A))
            desiredDirection += Vector3.left;
        if (Input.GetKey(KeyCode.D))
            desiredDirection += Vector3.right;
        if (Input.GetKey(KeyCode.W))
            desiredDirection += Vector3.forward;
        if (Input.GetKey(KeyCode.S))
            desiredDirection += Vector3.back;

        setWeaponRange();
        pointOfView( Color.white );
        MovePlayer(desiredDirection.normalized);

        if( GameObject.FindGameObjectWithTag( "virtualRotation" ).GetComponent<VirtualRotation>().doubleTouch || GameObject.FindGameObjectWithTag( "shootButton" ).GetComponent<stopWalking>().shootON ){
            if( weapons[weaponSelector].shoot() )
                pointOfView( Color.red );
            GameObject.FindGameObjectWithTag( "virtualRotation" ).GetComponent<VirtualRotation>().doubleTouch = false;
        }

    }

    private void setWeaponRange() {
        range.gameObject.transform.localScale = weapons[weaponSelector].weaponRange;
        float x = range.gameObject.transform.localScale.x / 5f;
        float y = range.gameObject.transform.localScale.y / 5f;
        secondRange.gameObject.transform.localScale = new Vector3(x, y, 0);
    }

    private void pointOfView(Color color){
        Vector3 mouseWorldPosition = Camera.main.ScreenToWorldPoint(new Vector3(
            Input.mousePosition.x,
            Input.mousePosition.y,
            -Camera.main.transform.position.z + Input.mousePosition.z));
        Debug.DrawLine( weapons[weaponSelector].transform.position, mouseWorldPosition, color );
    }

    public void weaponChange() {
        if (weaponSelector == (weapons.Length - 1))
                weaponSelector = 0;
            else
                weaponSelector += 1;

        foreach( Weapon weapon in weapons ) {
            if( weapon != weapons[weaponSelector] )
                weapon.gameObject.SetActive(false);
            else
                weapon.gameObject.SetActive( true );
        }
    }

}
