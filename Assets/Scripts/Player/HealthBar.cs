﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HealthBar : MonoBehaviour {

    Health health;
    GameObject gameNumber;
    GameObject icon;
    float life;
    float iconCooldown = 0.5f;
    float lastIconEffect = 0;
    private AudioSource heartbeatSound;
    private bool sound = true;

    // Use this for initialization
    void Awake () {
        health = GameObject.FindGameObjectWithTag("Player").GetComponentInChildren<Health>();
        gameNumber = GameObject.FindGameObjectWithTag("PlayerHPInfo");
        icon = GameObject.FindGameObjectWithTag( "iconHPBar" );
        heartbeatSound = gameObject.GetComponent<AudioSource>();
    }
	
	// Update is called once per frame
	void LateUpdate () {
        life = health.health / health.maxHealth;
        transform.localScale = new Vector3(life , 1, 1);
        gameNumber.GetComponent<Text>().text = Mathf.Round(life * 100) + "%";

        if( life <= 0.15f && (Time.time - lastIconEffect > iconCooldown) ) {
            lastIconEffect = Time.time;
            if( icon.activeInHierarchy )
                icon.SetActive( false );
            else
                icon.SetActive( true );
        }

        if( life <= 0.15f && sound ) {
            sound = false;
            heartbeatSound.Play();
        }

    }
}
