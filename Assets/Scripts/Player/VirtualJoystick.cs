﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class VirtualJoystick : MovingEntity, IDragHandler, IPointerDownHandler, IPointerUpHandler {

    private Image bgImg;
    public Image joystickImg;
    public Vector3 inputVector;
    public GameObject player;

    private void Start() {
        player = GameObject.FindGameObjectWithTag( "Player" );
        bgImg = GetComponent<Image>();
        joystickImg = transform.GetChild( 0 ).GetComponent<Image>();
    }

    public virtual void OnDrag( PointerEventData ped ) {
        Vector2 pos;
        if( RectTransformUtility.ScreenPointToLocalPointInRectangle( bgImg.rectTransform, ped.position, ped.pressEventCamera, out pos ) ) {
            pos.x = (pos.x / bgImg.rectTransform.sizeDelta.x);
            pos.y = (pos.y / bgImg.rectTransform.sizeDelta.y);

            inputVector = new Vector3( pos.x*4.5f , 0, pos.y*4.5f );
            inputVector = (inputVector.magnitude > 1.0f) ? inputVector.normalized : inputVector;

            joystickImg.rectTransform.anchoredPosition = new Vector3( inputVector.x * (bgImg.rectTransform.sizeDelta.x/3)
                                                                    , inputVector.z * (bgImg.rectTransform.sizeDelta.y / 3) );

            player.GetComponent<Player>().desiredDirection = Vector3.zero;
            if( (inputVector.x > 0 && inputVector.x < 0.2f || inputVector.x < 0 && inputVector.x > -0.2f) && inputVector.z > 0.975f ) { // solo arriba
                player.GetComponent<Player>().desiredDirection += Vector3.forward;
            }else if( (inputVector.x > -0.2f && inputVector.x < 0.2f) && inputVector.z < -0.975f ) { //solo abajo
                player.GetComponent<Player>().desiredDirection += Vector3.back;
            }else if( (inputVector.x < 0 && inputVector.x < -0.98f) && (inputVector.z < -0.08f || inputVector.z > -0.08f) ) { //solo izquierda
                player.GetComponent<Player>().desiredDirection += Vector3.left;
            }else if( (inputVector.z < 0.15f && inputVector.z > -0.1) ){ //solo derecha
                player.GetComponent<Player>().desiredDirection += Vector3.right;
            }else {
                if( inputVector.z > 0 )
                    player.GetComponent<Player>().desiredDirection += Vector3.forward;
                if( inputVector.z < 0 )
                    player.GetComponent<Player>().desiredDirection += Vector3.back;

                if( inputVector.x < 0 && inputVector.z > 0 ) // arriba izquierda
                    player.GetComponent<Player>().desiredDirection += Vector3.left; //Debug.Log( "arriba izquierda" );
                if( inputVector.x > 0 && inputVector.x < 1 && inputVector.z > 0 && inputVector.z < 1 ) // arriba derecha
                    player.GetComponent<Player>().desiredDirection += Vector3.right; //Debug.Log( "arriba derecha" );
                if( inputVector.x > 0 && inputVector.x < 1 && inputVector.z < 0 && inputVector.z > -1 ) // abajo derecha
                    player.GetComponent<Player>().desiredDirection += Vector3.right; //Debug.Log( "abajo derecha" );
                if( inputVector.x < 0 && inputVector.z < 0 ) // abajo izquierda
                    player.GetComponent<Player>().desiredDirection += Vector3.left; //Debug.Log( "abajo izquierda" );
            }

        }
    }

    public virtual void OnPointerUp( PointerEventData ped ) {
        inputVector = Vector3.zero;
        joystickImg.rectTransform.anchoredPosition = Vector3.zero;
        player.GetComponent<Player>().desiredDirection = Vector3.zero;
    }

    public virtual void OnPointerDown( PointerEventData ped ){
        OnDrag( ped );
    }

}
