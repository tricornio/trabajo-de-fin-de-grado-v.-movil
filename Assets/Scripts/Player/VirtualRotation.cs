﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class VirtualRotation : MovingEntity, IDragHandler, IPointerUpHandler, IPointerDownHandler {

    private Image bgImg;
    private Image joystickImg;
    private Vector3 inputVector;
    GameObject player;

    private void Start() {
        bgImg = GetComponent<Image>();
        joystickImg = transform.GetChild( 0 ).GetComponent<Image>();
    }

    public virtual void OnDrag( PointerEventData ped ) {
        Vector2 pos;
        if( RectTransformUtility.ScreenPointToLocalPointInRectangle( bgImg.rectTransform, ped.position, ped.pressEventCamera, out pos ) ) {
            pos.x = (pos.x / bgImg.rectTransform.sizeDelta.x);
            pos.y = (pos.y / bgImg.rectTransform.sizeDelta.y);

            inputVector = new Vector3( pos.x*4.5f , 0, pos.y*4.5f );
            inputVector = (inputVector.magnitude > 1.0f) ? inputVector.normalized : inputVector;

            joystickImg.rectTransform.anchoredPosition = new Vector3( inputVector.x * (bgImg.rectTransform.sizeDelta.x/3)
                                                                    , inputVector.z * (bgImg.rectTransform.sizeDelta.y / 3) );
            
        }






        player = GameObject.FindGameObjectWithTag( "Player" );
        var playerPlane = new Plane(Vector3.up, player.transform.position);

        // Generate a ray from the cursor position
        Camera camaraprincipal = Camera.main;
        camaraprincipal.transform.position = new Vector3( camaraprincipal.transform.position.x +9, camaraprincipal.transform.position.y, camaraprincipal.transform.position.z);

        Vector2 prueba = new Vector2( camaraprincipal.transform.position.x + 9, camaraprincipal.transform.position.z );
        //RectTransformUtility.ScreenPointToLocalPointInRectangle( bgImg.rectTransform, ped.position, ped.pressEventCamera, out prueba );

        var ray = camaraprincipal.ScreenPointToRay( fingerPosition.position );
        //Debug.Log( camaraprincipal.transform.position );
        //var ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        
        float hitdist = 0;
       
        if (playerPlane.Raycast(ray, out hitdist)) {
            // Get the point along the ray that hits the calculated distance.
            var targetPoint = ray.GetPoint(hitdist);

            // Determine the target rotation.  This is the rotation if the transform looks at the target point.
            var targetRotation = Quaternion.LookRotation(targetPoint - player.transform.position);

            // Smoothly rotate towards the target point.
            player.transform.rotation = Quaternion.Lerp( player.transform.rotation, targetRotation, rotationSpeed * Time.deltaTime);
        }
        
        //transform.position += direction * 5 * Time.deltaTime;

    }

    public virtual void OnPointerUp( PointerEventData ped ) {
        inputVector = Vector3.zero;
        joystickImg.rectTransform.anchoredPosition = Vector3.zero;
    }

    public virtual void OnPointerDown( PointerEventData ped ){
        OnDrag( ped );
    }

    public Touch fingerPosition;
    public bool doubleTouch = false;
    void Update() {
        if( Input.touchCount != 0 ) {
            fingerPosition = Input.GetTouch( 0 );
            for( int i = 1; i < Input.touchCount; ++i){
                if( Input.GetTouch( i ).position.x < fingerPosition.position.x )
                    fingerPosition = Input.GetTouch( i );
                if( Input.GetTouch( i ).tapCount >= 2 ) {
                    doubleTouch = true;
                }
            }
        }
    }

}

