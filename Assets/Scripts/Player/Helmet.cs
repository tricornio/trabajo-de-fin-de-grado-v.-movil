﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Helmet : MonoBehaviour {

    float lightCooldown = 5;
    float lastLightTime = 0;
    bool turnOff = false;
    bool lowHealth = false;
    GameObject lanterState;
    AudioSource[] audio;
    int randomTime;
    
    void Awake() {
        lanterState = GameObject.FindGameObjectWithTag( "LanterBarInfo" );
        audio = GetComponents<AudioSource>();
    }

    void Update () {
        if( gameObject.GetComponentInParent<Health>().health <= (gameObject.GetComponentInParent<Health>().maxHealth * 0.15) && !lowHealth ) {
            lowHealth = true;
            gameObject.GetComponent<Light>().enabled = false;
            gameObject.GetComponent<Light>().enabled = true;
        }

    }

    public void turonmobile() {
        if( !turnOff && !lowHealth ) {
            audio[1].Play();

            if( gameObject.GetComponent<Light>().enabled )
                gameObject.GetComponent<Light>().enabled = false;
            else
                gameObject.GetComponent<Light>().enabled = true; 

            StartCoroutine( "turnOnOffLight" );
        }

        if( gameObject.GetComponent<Light>().enabled )
            lanterState.GetComponent<Image>().color = Color.green;
        else
            lanterState.GetComponent<Image>().color = Color.red;
    }

    void LateUpdate() {
        if( Input.GetKeyUp( KeyCode.LeftControl ) || (gameObject.GetComponentInParent<Health>().health <= (gameObject.GetComponentInParent<Health>().maxHealth * 0.15)) ){
            if( gameObject.GetComponent<Light>().enabled )
                lanterState.GetComponent<Image>().color = Color.green;
            else
                lanterState.GetComponent<Image>().color = Color.red;
        }
    }

    IEnumerator turnOnOffLight(){
        while( gameObject.GetComponent<Light>().enabled && !lowHealth ){

            if( Time.time - lastLightTime > lightCooldown ){
                lastLightTime = Time.time;

                if( UnityEngine.Random.Range( 0, 3 ) == 2 ) {
                    audio[0].Play();
                    Invoke( "turnOffLight", 1 );
                }
                    
            }

            yield return 0;
        }
    }

    private void turnOffLight() {
        turnOff = true;
        gameObject.GetComponent<Light>().enabled = false;
        randomTime = UnityEngine.Random.Range( 1, 6 );
        Invoke( "playSound", randomTime - 1 );
        Invoke( "turnOn", randomTime );
    }

    private void playSound() {
        audio[0].Play();
    }

    private void turnOn() {
        turnOff = false;
        gameObject.GetComponent<Light>().enabled = true;
        StartCoroutine( "turnOnOffLight" );
    }

}
