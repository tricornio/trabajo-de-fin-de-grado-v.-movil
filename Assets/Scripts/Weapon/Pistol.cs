﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Pistol : Weapon {

    private AudioSource sound;

    public override bool shoot(){
        if( Time.time - lastShootTime > shootCooldown ){
            GameObject bullet = GameObject.Instantiate( bulletPrefab, transform.position, transform.rotation );
            bullet.GetComponent<Bullet>().damage = bulletDamage;
            lastShootTime = Time.time;
            sound.Play();
            return true;
        }
        return false;
    }

    void Start(){
        sound = GetComponent<AudioSource>();
        bulletDamage = 2f;
        shootCooldown = 0.6f;
        weaponRange = new Vector3( 20, 20, 1 );
    }

}
