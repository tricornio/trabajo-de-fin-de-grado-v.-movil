﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Shotgun : Weapon {

    public ParticleSystem lightEffect;
    public Light light;
    private int spreadX;
    private int spreadZ;

    public override bool shoot(){
        if (Time.time - lastShootTime > shootCooldown){

            lightEffect.Play();
            audio.Play();
            light.enabled = true;

            for (int i = 0; i < 35; i++) {
                //GameObject.Instantiate( bulletPrefab, transform.position, transform.rotation );
                spreadX = UnityEngine.Random.Range( -45, 45 );
                spreadZ = UnityEngine.Random.Range( -10, 10 );
                GameObject bullet = Instantiate( bulletPrefab, transform.position, transform.rotation );
                bullet.transform.Rotate( spreadX, 0, spreadZ );

                bullet.GetComponent<Bullet>().damage = bulletDamage;
            }
            
            lastShootTime = Time.time;
            Invoke( "turnOffLight", 0.1f );
            return true;
        }
        return false;
    }

    private void turnOffLight() { light.enabled = false; }

    // Use this for initialization
    void Start () {
        audio = GetComponent<AudioSource>();
        bulletDamage = 4;
        shootCooldown = 2f;
        weaponRange = new Vector3( 10, 10, 1 );
    }

}
