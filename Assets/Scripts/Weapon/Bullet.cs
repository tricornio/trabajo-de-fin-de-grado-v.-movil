﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : MonoBehaviour {

    public int speed = 100;
    Rigidbody rb;
    //public GameObject bulletPrefab;
    public float damage;

    void Start() {
        rb = GetComponent<Rigidbody>();
        rb.AddForce( transform.forward * speed, ForceMode.VelocityChange );
    }

    void OnTriggerEnter( Collider other ) {
        if( other.CompareTag( "Enemy" ) ){
            destroyBullet();
            //Debug.Log( "El daño es de: " + damage );
            other.SendMessage( "DamageTaken", damage );
        }  
    }

    void destroyBullet() {
        Destroy( gameObject );
    }

    void OnTriggerExit( Collider other ) {
        if( other.CompareTag( "WeaponRange" ) )
            destroyBullet();
    }

}
