﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Weapon : MonoBehaviour {

    public GameObject bulletPrefab;
    public AudioSource audio;
    public float shootCooldown;
    public float bulletDamage;
    protected float lastShootTime = 0;
    public Vector3 weaponRange;

    public abstract bool shoot();

	// Use this for initialization
	void Start () {

    }
	
	// Update is called once per frame
	void Update () {
        
    }
}
