﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class FlamethrowerState : MonoBehaviour {

    public bool flamethrowerState;

    void Awake() { flamethrowerState = true; }

	void LateUpdate() {
        if( flamethrowerState ){
            gameObject.GetComponent<Image>().color = Color.green;
        }else{
            gameObject.GetComponent<Image>().color = Color.red;
        }
    }

}
