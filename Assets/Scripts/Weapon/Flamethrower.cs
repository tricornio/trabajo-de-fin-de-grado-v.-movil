﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Flamethrower : Weapon{

    public ParticleSystem fireEffect;
    public Light light;
    private bool shooting = false;
    private float lastUseTime = 0;
    private float useCooldown = 10f;

    bool activator = true;
    bool music = true;
    GameObject stateUse;

    public override bool shoot() {
        return true;
    }

	// Use this for initialization
	void Start () {
        bulletDamage = 0.75f;
        audio = GetComponent<AudioSource>();
        shootCooldown = 0.25f;
        weaponRange = new Vector3( 12, 12, 1 );
        stateUse = GameObject.FindWithTag( "FlamethriwerBarInfo" );
    }

    // Update is called once per frame
    void Update() {

        // for music
        if( Time.time - lastUseTime <= useCooldown && Input.GetAxis( "Fire1" ) != 0 && !music ) {
            music = true;
            audio.Play();
        }else if( Input.GetAxis( "Fire1" ) == 0 && music ) { music = false; }

        // for light and particle system
        if( Input.GetAxis( "Fire1" ) != 0 && !shooting && activator ) {
            if( activator ) {
                lastUseTime = Time.time;
                Invoke( "desactivateState", 10 );
            }
            activator = false;

            StartCoroutine( "onFire" );
            Invoke( "resetUse", 13 ); // 7 - useCooldown = timepo de uso
        }else if( Input.GetAxis( "Fire1" ) == 0 && shooting ) {
            shooting = false;
            audio.Stop();
            fireEffect.Stop();
            light.enabled = false;
        }
        
    }

    private void desactivateState() {
        stateUse.GetComponent<FlamethrowerState>().flamethrowerState = false;
    }

    private void resetUse(){
        stateUse.GetComponent<FlamethrowerState>().flamethrowerState = true;
        activator = true;
        music = false;
    }

    IEnumerator onFire() {
        while( Time.time - lastUseTime <= useCooldown ) {
            if( Input.GetAxis( "Fire1" ) == 1 ){
                fireEffect.Play();
                light.enabled = true;
                shooting = true;
            }else {
                shooting = false;
                audio.Stop();
                fireEffect.Stop();
                light.enabled = false;
            }
            
            yield return 0;
        }
        shooting = false;
        audio.Stop();
        fireEffect.Stop();
        light.enabled = false;
    }

    void OnTriggerStay( Collider other ){
        if( other.CompareTag( "Enemy" ) && shooting )
            if( Time.time - lastShootTime > shootCooldown ){
                lastShootTime = Time.time;
                Debug.Log( "El daño es de: " + bulletDamage );
                other.SendMessage( "DamageTaken", bulletDamage );
            }
    }

}