﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ChangeScene : MonoBehaviour {

    public void changeScene( string scene ) {
        if( scene == "StartGameScene" )
            Time.timeScale = 1;
        Application.LoadLevel(scene);
    }

    public void playLevel1( string scene ) {
        gameObject.GetComponent<ActiveButton>().level2Canvas.SetActive( true );
        Invoke( "activeLevel2", 7f );
    }

    public void playTrainingLevel( string scene ) {
        gameObject.GetComponent<ActiveButton>().trainingLevelCanvas.SetActive( true );
        Invoke( "activeTrainingLevel", 7f );
    }

    public void exitGame() {
        Application.Quit();
    }

    public void activeLevel2() {
        Application.LoadLevel( "Level2" );
    }

    public void activeTrainingLevel() {
        Application.LoadLevel( "Level1" );
    }

    public void activeVideo() {
        GameObject[] videos = GameObject.FindGameObjectsWithTag( "Video" );
        
        foreach( GameObject video in videos ) {
            if( video != gameObject )
                foreach( Transform child in video.transform ) {
                    if( child.gameObject.GetComponent<Text>() == null ) {
                        //child.GetComponent<PlayVideos>().movie.Stop();
                        child.gameObject.SetActive( false );
                    }
                }
            else
                foreach( Transform child in video.transform ) { child.gameObject.SetActive( true ); }
        }

        //gameObject.GetComponentInChildren<PlayVideos>().movie.Play();
    }

}
