﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PauseManager : MonoBehaviour {

    GameObject pauseCanvas;

    void Awake() {
        pauseCanvas = GameObject.FindGameObjectWithTag("PauseCanvas");
        pauseCanvas.SetActive( false );
    }

	void Update () {
        if( Input.GetKeyUp( KeyCode.Escape ) ){
            if( Time.timeScale == 1 ) { // if not pause
                pauseCanvas.SetActive( true );
                Time.timeScale = 0;
            }else{ // if pause
                if( GameObject.FindGameObjectWithTag( "ObjectivesCanvas" ) )
                    GameObject.FindGameObjectWithTag( "ObjectivesCanvas" ).SetActive( false );
                if( GameObject.FindGameObjectWithTag( "ApuntesCanvas" ) )
                    GameObject.FindGameObjectWithTag( "ApuntesCanvas" ).SetActive( false );
                if( GameObject.FindGameObjectWithTag( "ControlsCanvas" ) )
                    GameObject.FindGameObjectWithTag( "ControlsCanvas" ).SetActive( false );

                pauseCanvas.SetActive( false );
                Time.timeScale = 1;
            }
        }
    }

}
