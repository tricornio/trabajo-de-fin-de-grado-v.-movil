﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ActiveButton : MonoBehaviour {

    GameObject trap;
    public GameObject level2Canvas;
    public GameObject trainingLevelCanvas;

    void Awake () {
        trap = GameObject.FindGameObjectWithTag( "FireTrap" );
        level2Canvas = GameObject.FindGameObjectWithTag( "Level2Canvas" );
        trainingLevelCanvas = GameObject.FindGameObjectWithTag( "TrainingLevel" );
        trap.SetActive( false );
        level2Canvas.SetActive( false );
        trainingLevelCanvas.SetActive( false );
    }
	
	// Update is called once per frame
	public void activeTrap () {
        if( trap.active ) {
            trap.SetActive( false );
            trap.GetComponentsInChildren<CapsuleCollider>()[0].radius = 0.5f;
            trap.GetComponentsInChildren<CapsuleCollider>()[1].radius = 0.5f;
        }
        else {
            trap.SetActive( true );
        }
            
    }
}
