﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FireDamage : MonoBehaviour {

    private float damageCooldown = 0.8f;
    private float lastDamageTime = 0;
    private float damage = 1.50f;
    private float lastExpansionTime = 0;
    private float expansionCooldown = 1;

    void OnTriggerStay( Collider other ){
        if( other.CompareTag( "Enemy" ) ) {
            if( Time.time - lastDamageTime > damageCooldown ) {
                lastDamageTime = Time.time;
                other.SendMessage( "DamageTaken", damage );
            }
        }
    }

    void Update() {
        if( Time.time - lastExpansionTime > expansionCooldown && (gameObject.GetComponent<CapsuleCollider>().radius <= 48) ) {
            lastExpansionTime = Time.time;
            gameObject.GetComponent<CapsuleCollider>().radius += 5;
        }
    }
    
}

